package com.example.sunbeam.mydeliverer.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


import com.example.sunbeam.mydeliverer.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_person,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent= new Intent(this,PersonInfoActivity.class);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }

    public void getThingDone(View view) {
        Intent intent = new Intent(this,GetThingsDoneActivity.class);
        startActivity(intent);
    }

    public void bringGrocery(View view) {
        Intent intent = new Intent(this,BringGroceryActivity.class);
        startActivity(intent);

    }

    public void sendPackage(View view) {
        Intent intent = new Intent(this,SendPackageActivity.class);
        startActivity(intent);
    }
}
