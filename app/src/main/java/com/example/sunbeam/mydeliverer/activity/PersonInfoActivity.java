package com.example.sunbeam.mydeliverer.activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.TextView;

import com.example.sunbeam.mydeliverer.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PersonInfoActivity extends BackArrowActivity {

    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.mobile)
    TextView mobile;
    @BindView(R.id.email)
    TextView email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person_info);
        ButterKnife.bind(this);

        loadprofile();
    }

    void loadprofile() {
        name.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("UserName",""));
        mobile.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("UserMobile",""));
        email.setText(PreferenceManager.getDefaultSharedPreferences(this).getString("UserEmail",""));
    }


}
