package com.example.sunbeam.mydeliverer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sunbeam.mydeliverer.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendPackageActivity extends BackArrowActivity {

    @BindView(R.id.pickup)
    EditText pickup;
    @BindView(R.id.delivery)
    EditText delivery;
    @BindView(R.id.content)
    EditText content;
    @BindView(R.id.instructions)
    EditText instructions;
    @BindView(R.id.textTotalAmount)
    TextView textTotalAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_package);
        ButterKnife.bind(this);
    }


    public void checkout(View view) {

        int UserID = PreferenceManager.getDefaultSharedPreferences(this).getInt("UserID", 0);


        if (pickup.getText().toString().isEmpty()) {
            Toast.makeText(this, "Pickup address is mandatory", Toast.LENGTH_SHORT).show();
        } else if (delivery.getText().toString().isEmpty()) {
            Toast.makeText(this, "Delivery address is mandatory", Toast.LENGTH_SHORT).show();
        } else if (content.getText().toString().isEmpty()) {
            Toast.makeText(this, "Package contents is mandatory", Toast.LENGTH_SHORT).show();
        } else {
            JsonObject body = new JsonObject();
            body.addProperty("Order_pickup_point", pickup.getText().toString());
            body.addProperty("Order_drop_point", delivery.getText().toString());
            body.addProperty("Order_type", content.getText().toString());
            body.addProperty("Order_Description",instructions.getText().toString());



            Ion.with(this)
                .load("POST",("http://192.168.0.3:4000/Orders/" + UserID))
                .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Toast.makeText(SendPackageActivity.this, "Order confirmed", Toast.LENGTH_SHORT).show();

                        }
                    });


    }
}

    public void openMapForDelivery(View view) {
        Intent intent = new Intent(this,MapActivity.class);
        startActivityForResult(intent,1);
    }



    public void openMapForPickup(View view) {
        Intent intent = new Intent(this,MapActivity.class);
        startActivityForResult(intent,1);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode==1){
            String Address = data.getStringExtra("Address");

            pickup.setText(Address);


           /* if(pickup==null) {
                pickup.setText(Address);
            }else{
                delivery.setText(Address);
            }*/
        }
    }
}
