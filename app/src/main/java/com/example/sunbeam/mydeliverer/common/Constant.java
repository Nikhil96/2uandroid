package com.example.sunbeam.mydeliverer.common;

public class Constant {

    public static final String SERVER_URL = "http://http://192.168.43.52:4000";

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";

    public static final String API_USER= "/User";
    public static final String API_DELIVERYPERSON= "/DeliveryPerson";
    public static final String API_ORDERS = "/Orders";


}
