package com.example.sunbeam.mydeliverer.common;

import android.util.Log;

public class Utils {

    private static String TAG = "Utils";

    public static String createUrl(String api) {
        String url = Constant.SERVER_URL + api;
        Log.d(TAG, "URL: " + url);
        return url;
    }
}
