package com.example.sunbeam.mydeliverer.activity;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sunbeam.mydeliverer.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BringGroceryActivity extends BackArrowActivity {

    @BindView(R.id.groceryList)
    EditText groceryList;
    @BindView(R.id.pickup)
    EditText pickup;
    @BindView(R.id.delivery)
    EditText delivery;
    @BindView(R.id.textTotalAmount)
    TextView textTotalAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bring_grocery);
        ButterKnife.bind(this);
    }

    public void checkout(View view) {

        int UserID = PreferenceManager.getDefaultSharedPreferences(this).getInt("UserID", 0);


        if (groceryList.getText().toString().isEmpty()) {
            Toast.makeText(this, "Grocery List is mandatory", Toast.LENGTH_SHORT).show();
        } else if (delivery.getText().toString().isEmpty()) {
            Toast.makeText(this, "Delivery address is mandatory", Toast.LENGTH_SHORT).show();
        } else {
            JsonObject body = new JsonObject();
            body.addProperty("Order_pickup_point", pickup.getText().toString());
            body.addProperty("Order_drop_point", delivery.getText().toString());
            body.addProperty("Order_type", "GroceryList");
            body.addProperty("Order_Description",groceryList.getText().toString());



            Ion.with(this)
                    .load("POST",("http://192.168.0.3:4000/Orders/" + UserID))
                    .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Toast.makeText(BringGroceryActivity.this, "Order confirmed", Toast.LENGTH_SHORT).show();

                        }
                    });


        }
    }
}
