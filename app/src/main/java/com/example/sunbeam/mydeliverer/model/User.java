package com.example.sunbeam.mydeliverer.model;

public class User {

    private int UserID;
    private String UserName;
    private String UserMobile;
    private String UserEmail;
    private String password;


    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserMobile() {
        return UserMobile;
    }

    public void setUserMobile(String userMobile) {
        UserMobile = userMobile;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                ", UserName='" + UserName+ '\'' +
                ", UserMobile='" +UserMobile + '\'' +
                ",UserEmail ='" + UserEmail+ '\'' +
                '}';
    }
}
