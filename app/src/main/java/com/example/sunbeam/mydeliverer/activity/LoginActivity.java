package com.example.sunbeam.mydeliverer.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sunbeam.mydeliverer.R;
import com.example.sunbeam.mydeliverer.common.Constant;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.BindView;
import butterknife.ButterKnife;


public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_login)
    AppCompatButton btnLogin;
    @BindView(R.id.link_signup)
    TextView linkSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        }


    public void singIn(View view)
    {
        if (inputEmail.getText().toString().isEmpty()) {
        Toast.makeText(this, "Email is mandatory", Toast.LENGTH_SHORT).show();
    } else if (inputPassword.getText().toString().isEmpty()) {
        Toast.makeText(this, "Password is mandatory", Toast.LENGTH_SHORT).show();
    } else {
        JsonObject body = new JsonObject();
        body.addProperty("UserEmail", inputEmail.getText().toString());
        body.addProperty("password", inputPassword.getText().toString());
       Log.d("My ids",inputEmail.getText().toString()+" "+inputPassword.getText().toString());

       Ion.with(this).load("POST","http://192.168.0.3:4000/User/login")
               .setJsonObjectBody(body)
               .asJsonObject()
               .setCallback(new FutureCallback<JsonObject>() {
                   @Override
                   public void onCompleted(Exception e, JsonObject result) {
                       if(result.get("status").getAsString().equals(Constant.SUCCESS)){
                           Toast.makeText(LoginActivity.this, "Successful login", Toast.LENGTH_SHORT).show();

                           JsonObject data = result.get("data").getAsJsonObject();

                           SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this);
                           preferences
                                   .edit()
                                   .putBoolean("loginStatus", true)
                                   .putInt("UserID", data.get("UserID").getAsInt())
                                   .putString("UserName",data.get("UserName").getAsString())
                                   .putString("UserMobile",data.get("UserMobile").getAsString())
                                   .putString("UserEmail",data.get("UserEmail").getAsString())
                                   .commit();

                            Intent intent= new Intent(LoginActivity.this,HomeActivity.class);
                            startActivity(intent);
                            finish();

                       }else {
                           Toast.makeText(LoginActivity.this, "Invalid Email or Password", Toast.LENGTH_SHORT).show();
                       }

                   }
               });
    }

    }

    public void singUp(View view) {
        Intent intent = new Intent(this, SignupActivity.class);
        startActivity(intent);
    }
}

