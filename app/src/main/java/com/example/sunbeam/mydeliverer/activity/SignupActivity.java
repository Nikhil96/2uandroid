package com.example.sunbeam.mydeliverer.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sunbeam.mydeliverer.R;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";

    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.input_mobile)
    EditText inputMobile;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    @BindView(R.id.link_login)
    TextView linkLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
    }


    public void signUp(View v) {
        if (inputName.getText().toString().isEmpty()) {
            Toast.makeText(this, "Name is mandatory", Toast.LENGTH_SHORT).show();
        } else if (inputEmail.getText().toString().isEmpty()) {
            Toast.makeText(this, "Email is mandatory", Toast.LENGTH_SHORT).show();
        } else if (inputPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "Password is mandatory", Toast.LENGTH_SHORT).show();
        } else if(inputMobile.getText().toString().isEmpty()){
            Toast.makeText(this, "Mobile Number is mandatory", Toast.LENGTH_SHORT).show();
        }else {
            JsonObject body = new JsonObject();
            body.addProperty("UserName", inputName.getText().toString());
            body.addProperty("UserEmail", inputEmail.getText().toString());
            body.addProperty("password", inputPassword.getText().toString());
            body.addProperty("UserMobile", inputMobile.getText().toString());


            Ion.with(this)
                    .load("POST", "http://192.168.0.3:4000/User/register")
                    .setJsonObjectBody(body)
                    .asJsonObject()
                    .setCallback(new FutureCallback<JsonObject>() {
                        @Override
                        public void onCompleted(Exception e, JsonObject result) {
                            Toast.makeText(SignupActivity.this, "Registration Successful", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                    });
        }
    }

    public void already(View view) {
        finish();
    }
}
